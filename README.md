# Akee TM

MVP do projeto Startup One FIAP

## Instruções

Essas instruções farão com que você tenha uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste.

### Pré-Requisitos

- JDK 8 e variável JAVA_HOME configurada.

- Maven 3 ou mais atual.

- Docker

### Instalação

Após a instalação das dependências acima, execute os comandos (na raiz do projeto) abaixo para gerar a imagem da aplicação

```
mvn install dockerfile:build
```

Concluído o build, execute:

```
docker-compose up -d
```

Pronto! A aplicação pode ser acessada através do ```localhost:80```

### Testar o serviço REST

A autenticação do serviço REST é feita através de JWT (JSON Web Token) e portanto é necessário solicitar um token antes da chamada do serviço que deseja consumir. Para solicitar o token, utilize o Postman com os seguintes parâmetros:

```
Método: POST
Url: http://localhost:80/oauth/token?grant_type=password&username=user&password=user
Authorization:
	Type: Basic Auth
	Username: my-trusted-client
	Password: secret
```
* Para fins de testes, estamos utlizando usuários e senhas fixos.

O serviço deverá retornar uma mensagem com o seguinte formato:
```
{
    "access_token": "89752034-e8b4-4b37-972d-ab7cca8d2775",
    "token_type": "bearer",
    "expires_in": 4999,
    "scope": "read write trust"
}
```
Copie o access_token e envie como parâmetro em uma nova requisição via Postman, desta vez informando no contexto qual serviço deseja consumir:
```
### Listar Eventos
Método: GET
Url: http://localhost:80/api/evento?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
```
O serviço deverá retornar um arquivo JSON conforme o exemplo abaixo:
```
{
    "eventos": [
        {
            "id": 2,
            "preco": 20,
            "estabelecimento": {
                "id": 1,
                "nome": "FIAP",
                "endereco": "Av Paulista, 112"
            },
            "categoria": {
                "id": 4,
                "descricao": "HACKATON"
            },
            "data": "2018-01-01",
            "nome": "Hackaton FIAP",
            "descricao": "Hackaton da FIAP junto a parceiros buscando novas startups",
            "evento_url": "/api/evento/2"
        }
    ]
}
```

### Cadastrar Categorias
Método: POST
Url: http://localhost:80/api/categoria?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
```
O body da requisição deverá ser "application/json" seguindo o seguinte modelo:
```
{
   "descricao":"<nome-da-categoria>"
}
```

### Cadastrar Estabelecimento
Método: POST
Url: http://localhost:80/api/estabelecimento?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
```
O body da requisição deverá ser "application/json" seguindo o seguinte modelo:
```
{
    "nome": "FIAP",
    "endereco": "Av Paulista, 112"
}
```

### Cadastrar Usuário
Método: POST
Url: http://localhost:80/api/usuario?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
```
O body da requisição deverá ser "application/json" seguindo o seguinte modelo:
```
{
	"email":"j.myaki@gmail.com",
	"nome":"Juliana Myaki",
	"ativo":1,
	"dataNascimento":"1986-08-13",
	"genero":"F",
	"username":"myakiju",
	"password":"myakiju"
}
```

### Pesquisar Usuário
Método: GET
Url: http://localhost:80/api/usuario/{id}?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
```
O serviço deverá retornar um arquivo JSON conforme o exemplo abaixo:
```
{
	"email":"j.myaki@gmail.com",
	"nome":"Juliana Myaki",
	"ativo":1,
	"dataNascimento":"1986-08-13",
	"genero":"F",
	"username":"myakiju"
}
```

### Adicionar ou Remover Evento Favorito
Método: POST
URLs
Adicionar: http://localhost:80/api/favorito/add/{usuarioId}/{eventoId}?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
Remover: http://localhost:80/api/favorito/remove/{usuarioId}/{eventoId}?access_token=89752034-e8b4-4b37-972d-ab7cca8d2775
```
O serviço deverá retornar um arquivo JSON conforme o exemplo abaixo:
```
{
{
    "id": 1,
    "email": "j.myaki@gmail.com",
    "nome": "Juliana Myaki",
    "ativo": true,
    "dataNascimento": "1986-08-13T03:00:00.000+0000",
    "genero": "F",
    "username": "myakiju",
    "favoritos": [
        {
            "id": 1,
            "preco": 0,
            "estabelecimento": {
                "id": 1,
                "nome": "FIAP",
                "endereco": "Av Paulista, 112"
            },
            "categoria": {
                "id": 3,
                "descricao": "FESTIVAL"
            },
            "data": "2018-08-30",
            "nome": "One Startup Challenge",
            "descricao": "Deixamos o TCC de lado Aqui você cria sua startup. Pessoas, tecnologia e inovação estão no centro de todas as grandes transformações.",
            "imagem": "https://www.dropbox.com/s/2qeyh7mhxew05gj/startupOne.jpg?dl=0",
            "evento_url": null
        }
    ]
}
}
```

## Desenvolvido com

* [Spring](http://www.spring.io/) - Web framework
* [Maven](https://maven.apache.org/) - Dependency management
* [Thymeleaf](http://thymeleaf.org/) - Java template engine
* [Docker](https://www.docker.com/) - Build, ship and run any app, anywhere!

## Autores

* **Alex Soares de Siqueira**
* **Gustavo Ballesté Prado**
* **Juliana Myaki Bueno da Silva**
* **Leandro Lourenço Rodrigues**
* **Vinicius Nunes Coscia**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
