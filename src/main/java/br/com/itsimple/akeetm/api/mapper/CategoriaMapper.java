package br.com.itsimple.akeetm.api.mapper;

import br.com.itsimple.akeetm.api.model.CategoriaDTO;
import br.com.itsimple.akeetm.entity.Categoria;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoriaMapper {

    CategoriaMapper INSTANCE = Mappers.getMapper(CategoriaMapper.class);

    CategoriaDTO categoriaParaCatagoriaDTO(Categoria categoria);

	Categoria categoriaDTOparaCategoria(CategoriaDTO eventoDTO);
	
	

}
