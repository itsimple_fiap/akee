package br.com.itsimple.akeetm.api.mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class MapperConfig {

    @Bean
    public CategoriaMapper categoriaMapper() {
        return CategoriaMapper.INSTANCE;
    }

    @Bean
    public EventoMapper eventoMapper() {
        return EventoMapper.INSTANCE;
    }
    
    @Bean
    public EstabelecimentoMapper estabelecimentoMapper() {
    	return EstabelecimentoMapper.INSTANCE;
    }
    
    @Bean
    public UsuarioMapper usuarioMapper() {
    	return UsuarioMapper.INSTANCE;
    }

}
