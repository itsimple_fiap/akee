package br.com.itsimple.akeetm.api.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EstabelecimentoListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<EstabelecimentoDTO> listaEstabelecimento;
	
}
