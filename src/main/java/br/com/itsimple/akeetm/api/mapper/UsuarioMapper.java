package br.com.itsimple.akeetm.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import br.com.itsimple.akeetm.api.model.UsuarioDTO;
import br.com.itsimple.akeetm.entity.Usuario;

@Mapper
public interface UsuarioMapper {
	
	UsuarioMapper INSTANCE = Mappers.getMapper(UsuarioMapper.class);

    UsuarioDTO usuarioParaUsuarioDTO(Usuario usuario);

    Usuario usuarioDTOParaUsuario(UsuarioDTO usuarioDTO);
}
