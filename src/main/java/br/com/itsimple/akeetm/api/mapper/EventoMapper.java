package br.com.itsimple.akeetm.api.mapper;

import br.com.itsimple.akeetm.api.model.EventoDTO;
import br.com.itsimple.akeetm.entity.Evento;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EventoMapper {

    EventoMapper INSTANCE = Mappers.getMapper(EventoMapper.class);

    EventoDTO eventoParaEventoDTO(Evento evento);

    Evento eventoDTOparaEvento(EventoDTO eventoDTO);
}
