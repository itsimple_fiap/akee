package br.com.itsimple.akeetm.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import br.com.itsimple.akeetm.api.model.EstabelecimentoDTO;
import br.com.itsimple.akeetm.entity.Estabelecimento;

@Mapper
public interface EstabelecimentoMapper {
	EstabelecimentoMapper INSTANCE = Mappers.getMapper(EstabelecimentoMapper.class);
	
	EstabelecimentoDTO estabelecimentoParaEstabelecimentoDTO(Estabelecimento estabelecimento);
	
	Estabelecimento estabelecimentoDTOparaEstabelecimento(EstabelecimentoDTO estabelecimentoDTO);
}
