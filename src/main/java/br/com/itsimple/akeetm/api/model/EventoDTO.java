package br.com.itsimple.akeetm.api.model;

import br.com.itsimple.akeetm.entity.Categoria;
import br.com.itsimple.akeetm.entity.Estabelecimento;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private float preco;
    private Estabelecimento estabelecimento;
    private Categoria categoria;
    private Date data;
    private String nome;
    private String descricao;
    private String imagem;

    @JsonProperty("evento_url")
    private String eventoUrl;

}
