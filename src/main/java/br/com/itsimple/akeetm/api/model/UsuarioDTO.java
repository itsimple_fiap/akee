package br.com.itsimple.akeetm.api.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@EqualsAndHashCode(exclude={"favoritos"})
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String email;
	private String nome;
	private boolean ativo;
	private Calendar dataNascimento;
	private String genero;
	private String username;
	
	//TODO implement encoding
	private String password;
	private Set<EventoDTO> favoritos = new HashSet<>();
	
	
}
