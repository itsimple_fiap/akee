package br.com.itsimple.akeetm.api.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EstabelecimentoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
    private Long id;
    private String nome;
    private String endereco;
    
    @JsonProperty("estabelecimento_url")
    private String estabelecimentoUrl;
}
