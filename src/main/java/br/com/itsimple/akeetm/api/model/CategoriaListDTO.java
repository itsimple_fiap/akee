package br.com.itsimple.akeetm.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class CategoriaListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    List<CategoriaDTO> categorias;
}
