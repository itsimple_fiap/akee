package br.com.itsimple.akeetm.config;

import br.com.itsimple.akeetm.entity.Login;
import br.com.itsimple.akeetm.entity.Role;
import br.com.itsimple.akeetm.repository.LoginRepository;
import br.com.itsimple.akeetm.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class AuthenticationManagerProvider extends WebSecurityConfigurerAdapter {

    @Autowired
    LoginRepository repository;

    @Autowired
    LoginService service;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        if (repository.count()==0)
            service.save(new Login("user", "user", Arrays.asList(new Role("USER"), new Role("ACTUATOR"))));
        auth.userDetailsService(userDetailsService(repository)).passwordEncoder(passwordEncoder());
    }

    private UserDetailsService userDetailsService(final LoginRepository repository) {
        return username -> new CustomUserDetails(repository.findByUsername(username));
    }

}
