package br.com.itsimple.akeetm.controller.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itsimple.akeetm.api.model.EstabelecimentoDTO;
import br.com.itsimple.akeetm.api.model.EstabelecimentoListDTO;
import br.com.itsimple.akeetm.service.EstabelecimentoService;

@RestController
@RequestMapping(EstabelecimentoController.BASE_URL)
public class EstabelecimentoController {
	
    public static final String BASE_URL = "/api/estabelecimento";
    
    private final EstabelecimentoService estabelecimentoService;

    public EstabelecimentoController(EstabelecimentoService estabelecimentoService) {
    	this.estabelecimentoService = estabelecimentoService;
    }
    
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public EstabelecimentoListDTO listAll() {
        return new EstabelecimentoListDTO(estabelecimentoService.getAll());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EstabelecimentoDTO getById(@PathVariable Long id){
        return estabelecimentoService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EstabelecimentoDTO createNew(@RequestBody EstabelecimentoDTO estabelecimentoDTO){
        return estabelecimentoService.createNew(estabelecimentoDTO);
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public EstabelecimentoDTO update(@PathVariable Long id, @RequestBody EstabelecimentoDTO estabelecimentoDTO){
        return estabelecimentoService.saveByDTO(id, estabelecimentoDTO);
    }

    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        estabelecimentoService.deleteById(id);
    }
}
