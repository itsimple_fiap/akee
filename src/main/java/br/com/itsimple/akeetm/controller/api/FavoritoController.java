package br.com.itsimple.akeetm.controller.api;

import br.com.itsimple.akeetm.api.model.UsuarioDTO;
import br.com.itsimple.akeetm.service.UsuarioService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(FavoritoController.BASE_URL)
public class FavoritoController {

    public static final String BASE_URL = "/api/favorito";

    private final UsuarioService usuarioService;

    public FavoritoController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }
    

    @PostMapping("/add/{id}/{eventoId}")
    @ResponseStatus(HttpStatus.OK)
    public UsuarioDTO addFavorite(@PathVariable Integer id, @PathVariable Long eventoId){
        return usuarioService.addFavorite(id, eventoId);
    }

    @PostMapping("/remove/{id}/{eventoId}")
    @ResponseStatus(HttpStatus.OK)
    public UsuarioDTO removeFavorite(@PathVariable Integer id, @PathVariable Long eventoId){
        return usuarioService.removeFavorite(id, eventoId);
    }
}
