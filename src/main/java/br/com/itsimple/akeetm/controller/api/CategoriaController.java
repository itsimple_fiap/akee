package br.com.itsimple.akeetm.controller.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itsimple.akeetm.api.model.CategoriaDTO;
import br.com.itsimple.akeetm.api.model.CategoriaListDTO;
import br.com.itsimple.akeetm.service.CategoriaService;

@RestController
@RequestMapping(CategoriaController.BASE_URL)
public class CategoriaController {

    public static final String BASE_URL = "api/categoria";

    private final CategoriaService categoriaService;

    public CategoriaController(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CategoriaListDTO listar() {
        return new CategoriaListDTO(categoriaService.findAll());
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CategoriaDTO createNew(@RequestBody CategoriaDTO categoriaDTO){
        return categoriaService.createNew(categoriaDTO);
    }
}
