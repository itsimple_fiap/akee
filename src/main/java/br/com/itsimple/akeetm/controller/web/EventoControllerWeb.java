package br.com.itsimple.akeetm.controller.web;

import br.com.itsimple.akeetm.entity.Categoria;
import br.com.itsimple.akeetm.entity.Estabelecimento;
import br.com.itsimple.akeetm.entity.Evento;
import br.com.itsimple.akeetm.service.web.CategoriaServiceWeb;
import br.com.itsimple.akeetm.service.web.EstabelecimentoServiceWeb;
import br.com.itsimple.akeetm.service.web.EventoServiceWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.apache.log4j.Logger;

@Controller
@RequestMapping("/")
public class EventoControllerWeb {

    private static final Logger logger = Logger.getLogger(EventoControllerWeb.class);

    @Autowired
    EventoServiceWeb eventoService;

    @Autowired
    CategoriaServiceWeb categoriaService;

    @Autowired
    EstabelecimentoServiceWeb estabelecimentoService;

    @GetMapping("/")
    public String findAll(Model model) {
        model.addAttribute("eventos", eventoService.findAll());

        return "evento";
    }

    @GetMapping("/adicionar")
    public String adicionar(Model model) {
        model.addAttribute("evento", new Evento());

        return "eventoCadastro";
    }

    @GetMapping("/editar/{id}")
    public String editar(@PathVariable String id, Model model) {
        model.addAttribute("evento", eventoService.findOne(Long.valueOf(id)));

        return "eventoCadastro";
    }

    @GetMapping("/excluir/{id}")
    public String excluir(@PathVariable String id) {
        eventoService.delete(Long.valueOf(id));

        return "redirect:/";
    }

    @PostMapping("/gravar")
    public String gravar(@ModelAttribute Evento evento) {
        eventoService.save(evento);

        return "redirect:/";
    }

    @ModelAttribute("categorias")
    public List<Categoria> listaCategorias() {

        return categoriaService.findAll();
    }

    @ModelAttribute("estabelecimentos")
    public List<Estabelecimento> listaEstabelecimento() {

        return estabelecimentoService.findAll();
    }

}
