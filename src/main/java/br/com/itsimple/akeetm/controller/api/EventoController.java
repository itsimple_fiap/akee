package br.com.itsimple.akeetm.controller.api;

import br.com.itsimple.akeetm.api.model.EventoDTO;
import br.com.itsimple.akeetm.api.model.EventoListDTO;
import br.com.itsimple.akeetm.service.EventoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EventoController.BASE_URL)
public class EventoController {

    public static final String BASE_URL = "/api/evento";

    private final EventoService eventoService;

    public EventoController(EventoService eventoService) {
        this.eventoService = eventoService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public EventoListDTO listAll() {
        return new EventoListDTO(eventoService.getAll());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EventoDTO getById(@PathVariable Long id){
        return eventoService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EventoDTO createNew(@RequestBody EventoDTO eventoDTO){
        return eventoService.createNew(eventoDTO);
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public EventoDTO update(@PathVariable Long id, @RequestBody EventoDTO eventoDTO){
        return eventoService.saveByDTO(id, eventoDTO);
    }

    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        eventoService.deleteById(id);
    }
}
