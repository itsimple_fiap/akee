package br.com.itsimple.akeetm.controller.api;

import br.com.itsimple.akeetm.api.model.UsuarioDTO;
import br.com.itsimple.akeetm.service.UsuarioService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(UsuarioController.BASE_URL)
public class UsuarioController {

    public static final String BASE_URL = "/api/usuario";

    private final UsuarioService usuarioService;

    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

//    @GetMapping
//    @ResponseStatus(HttpStatus.OK)
//    public UsuarioListDTO listAll() {
//        return new UsuarioListDTO(usuarioService.getAll());
//    }
    

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UsuarioDTO getById(@PathVariable Integer id){
        return usuarioService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UsuarioDTO createNew(@RequestBody UsuarioDTO usuarioDTO){
        return usuarioService.createNew(usuarioDTO);
    }

    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UsuarioDTO update(@PathVariable Integer id, @RequestBody UsuarioDTO usuarioDTO){
        return usuarioService.saveByDTO(id, usuarioDTO);
    }

    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer id){
        usuarioService.deleteById(id);
    }
}
