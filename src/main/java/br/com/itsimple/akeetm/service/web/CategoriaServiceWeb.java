package br.com.itsimple.akeetm.service.web;

import br.com.itsimple.akeetm.entity.Categoria;
import br.com.itsimple.akeetm.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaServiceWeb {

    @Autowired
    CategoriaRepository categoriaRepository;

    public List<Categoria> findAll() {

        return categoriaRepository.findAll();

    }
}

