package br.com.itsimple.akeetm.service;

import br.com.itsimple.akeetm.entity.Login;
import br.com.itsimple.akeetm.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginRepository repository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public void save(Login login){
        login.setPassword(passwordEncoder.encode(login.getPassword()));
        repository.save(login);
    }

}
