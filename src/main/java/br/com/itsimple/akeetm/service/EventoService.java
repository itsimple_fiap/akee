package br.com.itsimple.akeetm.service;

import java.util.List;

import br.com.itsimple.akeetm.api.model.EventoDTO;

public interface EventoService {

    List<EventoDTO> getAll();

    EventoDTO getById(Long id);

    EventoDTO createNew(EventoDTO customerDTO);

    EventoDTO saveByDTO(Long id, EventoDTO customerDTO);

    void deleteById(Long id);
    
}
