package br.com.itsimple.akeetm.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itsimple.akeetm.api.mapper.CategoriaMapper;
import br.com.itsimple.akeetm.api.model.CategoriaDTO;
import br.com.itsimple.akeetm.entity.Categoria;
import br.com.itsimple.akeetm.repository.CategoriaRepository;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    CategoriaRepository categoriaRepository;

    @Autowired
    CategoriaMapper categoriaMapper;

    public List<CategoriaDTO> findAll() {

        return categoriaRepository.findAll()
                .stream()
                .map(categoriaMapper::categoriaParaCatagoriaDTO)
                .collect(Collectors.toList());
    }
    
    public CategoriaDTO createNew(CategoriaDTO eventoDTO) {
        return saveAndReturnDTO(categoriaMapper.categoriaDTOparaCategoria(eventoDTO));
    }
    
    private CategoriaDTO saveAndReturnDTO(Categoria categoria) {
    	Categoria eventoSalvo = categoriaRepository.save(categoria);
        CategoriaDTO retornoDto = categoriaMapper.categoriaParaCatagoriaDTO(eventoSalvo);
        return retornoDto;
    }
}

