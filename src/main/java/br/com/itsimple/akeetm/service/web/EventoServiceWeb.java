package br.com.itsimple.akeetm.service.web;

import br.com.itsimple.akeetm.entity.Evento;
import br.com.itsimple.akeetm.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventoServiceWeb {

    @Autowired
    private EventoRepository repository;

    public List<Evento> findAll() {
        return repository.findAll();
    }

    public Evento findOne(Long id) {
        return repository.getOne(id);
    }

    public Evento save(Evento evento) {
        return repository.saveAndFlush(evento);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
