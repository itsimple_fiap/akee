package br.com.itsimple.akeetm.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itsimple.akeetm.api.mapper.EstabelecimentoMapper;
import br.com.itsimple.akeetm.api.model.EstabelecimentoDTO;
import br.com.itsimple.akeetm.controller.api.EstabelecimentoController;
import br.com.itsimple.akeetm.entity.Estabelecimento;
import br.com.itsimple.akeetm.repository.EstabelecimentoRepository;

@Service
public class EstabelecimentoServiceImpl implements EstabelecimentoService {

	@Autowired
	EstabelecimentoRepository estabelecimentoRepository;
	
	@Autowired
	EstabelecimentoMapper estabelecimentoMapper;
	
	@Override
	public List<EstabelecimentoDTO> getAll() {
		return estabelecimentoRepository
				.findAll()
				.stream()
				.map(estabelecimentoMapper::estabelecimentoParaEstabelecimentoDTO)
				.collect(Collectors.toList());
	}

    @Override
    public EstabelecimentoDTO getById(Long id) {

        return estabelecimentoRepository.findById(id)
                .map(estabelecimentoMapper::estabelecimentoParaEstabelecimentoDTO)
                .map(estabelecimentoDTO -> {
                    estabelecimentoDTO.setEstabelecimentoUrl(getEstabelecimentorUrl(id));
                    return estabelecimentoDTO;
                })
                .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public EstabelecimentoDTO createNew(EstabelecimentoDTO estabelecimentoDTO) {
        return saveAndReturnDTO(estabelecimentoMapper.estabelecimentoDTOparaEstabelecimento(estabelecimentoDTO));
    }

    private EstabelecimentoDTO saveAndReturnDTO(Estabelecimento estabelecimento) {
        Estabelecimento estabelecimentoSalvo = estabelecimentoRepository.save(estabelecimento);
        EstabelecimentoDTO retornoDto = estabelecimentoMapper.estabelecimentoParaEstabelecimentoDTO(estabelecimentoSalvo);
        retornoDto.setEstabelecimentoUrl(getEstabelecimentorUrl(estabelecimentoSalvo.getId()));
        return retornoDto;
    }

    @Override
    public EstabelecimentoDTO saveByDTO(Long id, EstabelecimentoDTO estabelecimentoDTO) {
        Estabelecimento estabelecimento = estabelecimentoMapper.estabelecimentoDTOparaEstabelecimento(estabelecimentoDTO);
        estabelecimento.setId(id);
        return saveAndReturnDTO(estabelecimento);
    }

    @Override
    public void deleteById(Long id) {
        estabelecimentoRepository.deleteById(id);
    }

    private String getEstabelecimentorUrl(Long id) {
        return EstabelecimentoController.BASE_URL + "/" + id;
    }
	
}
