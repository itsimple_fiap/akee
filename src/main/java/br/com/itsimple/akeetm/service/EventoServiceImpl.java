package br.com.itsimple.akeetm.service;

import br.com.itsimple.akeetm.api.mapper.EventoMapper;
import br.com.itsimple.akeetm.api.model.EventoDTO;
import br.com.itsimple.akeetm.controller.api.EventoController;
import br.com.itsimple.akeetm.entity.Evento;
import br.com.itsimple.akeetm.repository.CategoriaRepository;
import br.com.itsimple.akeetm.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventoServiceImpl implements EventoService {

    @Autowired
    EventoRepository eventoRepository;

    @Autowired
    EventoMapper eventoMapper;

    @Autowired
    CategoriaRepository categoriaRepository;

    @Override
    public List<EventoDTO> getAll() {
        return eventoRepository
                .findAll()
                .stream()
                .map(evento -> {
                    EventoDTO eventoDTO = eventoMapper.eventoParaEventoDTO(evento);
                    eventoDTO.setEventoUrl(getEventorUrl(evento.getId()));
                    return eventoDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public EventoDTO getById(Long id) {

        return eventoRepository.findById(id)
                .map(eventoMapper::eventoParaEventoDTO)
                .map(eventoDTO -> {
                    eventoDTO.setEventoUrl(getEventorUrl(id));
                    return eventoDTO;
                })
                .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public EventoDTO createNew(EventoDTO eventoDTO) {
        return saveAndReturnDTO(eventoMapper.eventoDTOparaEvento(eventoDTO));
    }

    private EventoDTO saveAndReturnDTO(Evento evento) {
        Evento eventoSalvo = eventoRepository.save(evento);
        EventoDTO retornoDto = eventoMapper.eventoParaEventoDTO(eventoSalvo);
        retornoDto.setEventoUrl(getEventorUrl(eventoSalvo.getId()));
        return retornoDto;
    }

    @Override
    public EventoDTO saveByDTO(Long id, EventoDTO eventoDTO) {
        Evento evento = eventoMapper.eventoDTOparaEvento(eventoDTO);
        evento.setId(id);
        return saveAndReturnDTO(evento);
    }

    @Override
    public void deleteById(Long id) {
        eventoRepository.deleteById(id);
    }

    private String getEventorUrl(Long id) {
        return EventoController.BASE_URL + "/" + id;
    }

}
