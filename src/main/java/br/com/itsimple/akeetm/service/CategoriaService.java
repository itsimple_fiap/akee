package br.com.itsimple.akeetm.service;

import br.com.itsimple.akeetm.api.model.CategoriaDTO;

import java.util.List;

public interface CategoriaService {

    List<CategoriaDTO> findAll();

	CategoriaDTO createNew(CategoriaDTO categoriaDTO);
}
