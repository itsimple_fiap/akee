package br.com.itsimple.akeetm.service;

import java.util.List;

import br.com.itsimple.akeetm.api.model.EstabelecimentoDTO;

public interface EstabelecimentoService  {
	
	List<EstabelecimentoDTO> getAll();
	
    EstabelecimentoDTO getById(Long id);

    EstabelecimentoDTO createNew(EstabelecimentoDTO customerDTO);

    EstabelecimentoDTO saveByDTO(Long id, EstabelecimentoDTO customerDTO);

    void deleteById(Long id);
	
}
