package br.com.itsimple.akeetm.service;

import java.util.List;

import br.com.itsimple.akeetm.api.model.EventoDTO;
import br.com.itsimple.akeetm.api.model.UsuarioDTO;

public interface UsuarioService {

	//TODO only for admins
    List<UsuarioDTO> getAll();

    //TODO only for admins or user can only see his own user information
    UsuarioDTO getById(Integer id);

    UsuarioDTO addFavorite(Integer usuarioId, Long eventoId);

    UsuarioDTO removeFavorite(Integer usuarioId, Long eventoId);

    UsuarioDTO createNew(UsuarioDTO usuarioDTO);

	UsuarioDTO saveByDTO(Integer id, UsuarioDTO usuarioDTO);

	void deleteById(Integer id);

}
