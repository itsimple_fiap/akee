package br.com.itsimple.akeetm.service.web;

import br.com.itsimple.akeetm.entity.Estabelecimento;
import br.com.itsimple.akeetm.repository.EstabelecimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstabelecimentoServiceWeb {

    @Autowired
    EstabelecimentoRepository repository;

    public List<Estabelecimento> findAll() {
        return repository.findAll();
    }
}
