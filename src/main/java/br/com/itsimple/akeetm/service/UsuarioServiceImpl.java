package br.com.itsimple.akeetm.service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itsimple.akeetm.api.mapper.EventoMapper;
import br.com.itsimple.akeetm.api.mapper.UsuarioMapper;
import br.com.itsimple.akeetm.api.model.EventoDTO;
import br.com.itsimple.akeetm.api.model.UsuarioDTO;
import br.com.itsimple.akeetm.entity.Usuario;
import br.com.itsimple.akeetm.repository.EventoRepository;
import br.com.itsimple.akeetm.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioMapper usuarioMapper;

	@Autowired
	private EventoRepository eventoRepository;

	@Autowired
	private EventoMapper eventoMapper;

	@Override
	public List<UsuarioDTO> getAll() {
		return usuarioRepository.findAll().stream().map(usuario -> {
			UsuarioDTO usuarioDTO = usuarioMapper.usuarioParaUsuarioDTO(usuario);
			return usuarioDTO;
		}).collect(Collectors.toList());
	}

	@Override
	public UsuarioDTO getById(Integer id) {

		return usuarioRepository.findById(id).map(usuarioMapper::usuarioParaUsuarioDTO).map(usuarioDTO -> {
			return usuarioDTO;
		}).orElseThrow(ResourceNotFoundException::new);
	}

	@Override
	public UsuarioDTO createNew(UsuarioDTO usuarioDTO) {
		return saveAndReturnDTO(usuarioMapper.usuarioDTOParaUsuario(usuarioDTO));
	}

	private UsuarioDTO saveAndReturnDTO(Usuario usuario) {
		Usuario usuarioSalvo = usuarioRepository.save(usuario);
		UsuarioDTO retornoDto = usuarioMapper.usuarioParaUsuarioDTO(usuarioSalvo);
		return retornoDto;
	}

	@Override
	public UsuarioDTO saveByDTO(Integer id, UsuarioDTO usuarioDTO) {
		Usuario usuario = usuarioMapper.usuarioDTOParaUsuario(usuarioDTO);
		usuario.setId(id);
		return saveAndReturnDTO(usuario);
	}

	@Override
	public void deleteById(Integer id) {
		usuarioRepository.deleteById(id);
	}

	@Override
	public UsuarioDTO addFavorite(Integer usuarioId, Long eventoId) {
		UsuarioDTO usuarioDTO = getById(usuarioId);

		EventoDTO eventoParaAdicionar = getEventoById(eventoId);
		
		if(usuarioDTO.getFavoritos() == null)
			usuarioDTO.setFavoritos(new HashSet<EventoDTO>());
		
		usuarioDTO.getFavoritos().add(eventoParaAdicionar);

		return saveByDTO(usuarioId, usuarioDTO);
	}

	@Override
	public UsuarioDTO removeFavorite(Integer usuarioId, Long eventoId) {
		
		UsuarioDTO usuarioDTO = getById(usuarioId);

		EventoDTO eventoParaRemover = usuarioDTO.getFavoritos().stream()
				.filter(evento -> eventoId.equals(evento.getId())).findAny().orElse(null);

		if (eventoParaRemover != null)
			usuarioDTO.getFavoritos().remove(eventoParaRemover);

		return saveByDTO(usuarioId, usuarioDTO);
	}

	private EventoDTO getEventoById(Long id) {

		return eventoRepository.findById(id).map(eventoMapper::eventoParaEventoDTO).map(eventoDTO -> {
			return eventoDTO;
		}).orElseThrow(ResourceNotFoundException::new);
	}

}
