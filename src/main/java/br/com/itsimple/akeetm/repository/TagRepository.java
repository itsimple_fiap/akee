package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {

}
