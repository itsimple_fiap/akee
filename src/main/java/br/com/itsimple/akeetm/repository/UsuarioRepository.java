package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
