package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Estabelecimento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstabelecimentoRepository extends JpaRepository<Estabelecimento, Long> {

}
