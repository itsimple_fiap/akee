package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Promotor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromotorRepository extends JpaRepository<Promotor, Long> {

}
