package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Login;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRepository extends JpaRepository<Login,Long> {
    Login findByUsername(String username);
}
