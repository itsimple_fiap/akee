package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
