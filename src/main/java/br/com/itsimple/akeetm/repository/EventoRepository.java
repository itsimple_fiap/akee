package br.com.itsimple.akeetm.repository;

import br.com.itsimple.akeetm.entity.Evento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventoRepository extends JpaRepository<Evento, Long> {

}
