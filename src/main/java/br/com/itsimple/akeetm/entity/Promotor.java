package br.com.itsimple.akeetm.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_promotor", schema = "akeetm")
public class Promotor {

    @Id
    @Column(name = "id_promotor")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", foreignKey = @ForeignKey(name = "fk_tb_pro_usu"), nullable = false)
    private Usuario usuario;

//    @OneToMany(mappedBy = "promotor")
//    private Set<Evento> eventos;

}
