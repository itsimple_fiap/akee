package br.com.itsimple.akeetm.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.itsimple.akeetm.api.model.UsuarioDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude={"usuarios"})
@Entity
@Table(name = "tb_evento", schema = "akeetm")
public class Evento {

	@Id
	@Column(name = "id_evento")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "fl_preco", nullable = false)
	private float preco;

	@ManyToOne(targetEntity = Estabelecimento.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_estabelecimento", foreignKey = @ForeignKey(name = "fk_tb_eve_est"), nullable = true)
	private Estabelecimento estabelecimento;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_categoria", foreignKey = @ForeignKey(name = "fk_tb_eve_cat"), nullable = true)
	private Categoria categoria;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_promotor", foreignKey = @ForeignKey(name = "fk_tb_eve_pro"), nullable = true)
	private Promotor promotor;

	// @OneToMany(mappedBy = "evento")
	// private Set<Cliente> clientes;

	// @OneToMany(mappedBy = "evento")
	// private Set<Periodo> periodos;

	@Column(name = "data")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date data;

	// @ManyToMany(fetch = FetchType.LAZY)
	// @JoinTable(name = "tb_tag_evento", joinColumns = @JoinColumn(name =
	// "id_evento"), inverseJoinColumns = @JoinColumn(name = "id_tag"))
	// private Set<Tag> tags;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "favoritos")
	private Set<Usuario> usuarios = new HashSet<>();

	@Column(name = "nm_evento", nullable = false)
	private String nome;

	@Column(name = "url_imagem")
	private String imagem;

	@Column(name = "ds_evento")
	private String descricao;

}
