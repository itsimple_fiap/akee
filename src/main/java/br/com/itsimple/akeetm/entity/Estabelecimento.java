package br.com.itsimple.akeetm.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_estabelecimento", schema = "akeetm")
public class Estabelecimento {

    @Id
    @Column(name = "id_estabelecimento")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nm_estabelecimento", length = 80, nullable = false)
    private String nome;

    @Column(name = "ds_endereco", length = 80, nullable = false)
    private String endereco;

//    @OneToMany(mappedBy = "estabelecimento")
//    private Set<Evento> eventos;

//    @ManyToOne
//    @JoinColumn(name = "id_categoria", unique = true, foreignKey = @ForeignKey(name = "fk_tb_est_cat"), nullable = true)
//    private Categoria categoria;

}
