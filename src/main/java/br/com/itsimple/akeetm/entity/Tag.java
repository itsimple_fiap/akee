package br.com.itsimple.akeetm.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "tb_tag", schema = "akeetm")
public class Tag {

    @Id
    @Column(name = "id_tag")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "ds_tag", unique = true, nullable = false)
    private String descricao;

    @ManyToMany
    @JoinTable(name = "tb_tag_evento", joinColumns = @JoinColumn(name = "id_tag"), inverseJoinColumns = @JoinColumn(name = "id_evento"))
    private Set<Evento> eventos;

}
