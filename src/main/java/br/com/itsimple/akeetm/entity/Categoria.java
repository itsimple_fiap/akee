package br.com.itsimple.akeetm.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_categoria", schema = "akeetm")
public class Categoria {

    @Id
    @Column(name = "id_categoria")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ds_categoria", length = 30, unique = true, nullable = false)
    private String descricao;

//    @OneToMany(mappedBy = "categoria")
//    private Set<Estabelecimento> estabelecimentos;
//
//    @OneToMany(mappedBy = "categoria")
//    private Set<Evento> eventos;

//    @ManyToMany
//    @JoinTable(name = "tb_interesse", joinColumns = @JoinColumn(name = "id_categoria"), inverseJoinColumns = @JoinColumn(name = "id_cliente"))
//    private Set<Cliente> clientes;

}
