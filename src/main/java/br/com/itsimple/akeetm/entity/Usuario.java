package br.com.itsimple.akeetm.entity;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.itsimple.akeetm.api.model.UsuarioDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude={"favoritos"})
@Entity
@Table(name = "tb_usuario", schema = "akeetm")
public class Usuario {

	@Id
	@Column(name = "id_usuario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; //TODO alterar pra LONG

	@Column(name = "ds_email", length = 80, nullable = false)
	private String email;

	@Column(name = "nm_usuario", length = 150, nullable = false)
	private String nome;

	@Column(name = "fl_ativo", nullable = false)
	private boolean ativo;

	@Temporal(TemporalType.DATE)
	@Column(name = "dt_nascimento", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Calendar dataNascimento;

	@Column(name = "nm_genero", length = 1, nullable = true)
	private String genero;

	@Column(name = "ds_username", length = 50, nullable = false)
	private String username;

	@Column(name = "ds_senha", length = 60, nullable = false)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "tb_favorito", joinColumns = { @JoinColumn(name = "usuario_id") }, inverseJoinColumns = {
			@JoinColumn(name = "evento_id") })
	private Set<Evento> favoritos = new HashSet<Evento>();

}
