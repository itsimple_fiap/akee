package br.com.itsimple.akeetm.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "tb_cliente", schema = "akeetm")
public class Cliente {

	@Id
	@Column(name = "id_cliente")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@OneToOne
	@JoinColumn(name = "id_usuario", foreignKey = @ForeignKey(name = "fk_tb_cli_usu"), nullable = false)
	private Usuario usuario;

//	@ManyToOne
//	@JoinColumn(name = "id_evento", foreignKey = @ForeignKey(name = "fk_tb_cli_eve"), nullable = false)
//	private Evento evento;
//
//	@ManyToMany
//	@JoinTable(name = "tb_interesse",
//			joinColumns = @JoinColumn(name = "id_cliente"),
//			inverseJoinColumns = @JoinColumn(name = "id_categoria"))
//	private Set<Categoria> categorias;

}
