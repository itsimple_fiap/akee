package br.com.itsimple.akeetm.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "tb_role", schema = "akeetm")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ds_nome")
    String nome;

    public Role(String nome) {
        this.nome = nome;
    }
}
