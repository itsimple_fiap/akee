INSERT INTO tb_categoria
  (id_categoria, ds_categoria)
VALUES
  (1, 'HACKATON'),
  (2, 'SHOW'),
  (3, 'FESTIVAL'),
  (4, 'CONGRESSO'),
  (5, 'ESPORTE');

INSERT INTO tb_estabelecimento
  (id_estabelecimento, ds_endereco, nm_estabelecimento)
VALUES
  (1, 'Av Paulista, 112', 'FIAP'),
  (2, 'Rua Coronel Oscar Porto, 70', 'Google Campus');
